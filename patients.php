<?php
//this is php file for creating table patients and values without manually entering  into MariaDB/we can use also the add button for adding patients

require "database.php";
//name, email, phone number, address, any known medical condition, blood type.
$sql = "CREATE TABLE patients (
    id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    patient_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    phone_number VARCHAR(255) NOT NULL,
    address_type VARCHAR(255) NOT NULL,
    medical_condition VARCHAR(255) NOT NULL,
    blood_type VARCHAR(255) NOT NULL,
    doctor_id int unsigned not null,
    constraint foreign key(doctor_id) references doctors(id))";
if ($pdo->query($sql) === TRUE) {
    echo "New table created successfully";
}
$sql="INSERT INTO patients (id, patient_name, email,phone_number,address_type,medical_condition,blood_type,doctor_id)
VALUES ('1','patient1','patient@test.com','25352','absb 1b','diabetes','ab+','1')";
if ($pdo->query($sql) === TRUE) {
    echo "New record created successfully";
}
$sql="INSERT INTO patients (id, patient_name, email,phone_number,address_type,medical_condition,blood_type,doctor_id)
VALUES ('2','patient2','patient2@test.com','192383373','helsinki 1b','blood pressure','0-','1')";
if ($pdo->query($sql) === TRUE) {
    echo "New record created successfully";
}

$sql="INSERT INTO patients (id, patient_name, email,phone_number,address_type,medical_condition,blood_type,doctor_id)
VALUES ('3','patient3','patient3@test.com','1239982','skopje 11b','heart failure','0+','1')";
if ($pdo->query($sql) === TRUE) {
    echo "New record created successfully";
}