<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Patients page</title>
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <style>
     #div-search{
          float: right;
     }
    </style>
</head>
<body>
     <div class="container">
             <div class="row">
                  <div class="col-md-12">
                         <h2 class="text-center">Patient List</h2>
                         <div id="message"></div>
                         <button type="button" name="add" id="add" data-toggle="modal" data-target="#add_modal" class="btn btn-success">Add</button> 
                              <!--Insert Modal-->
                              <div id="add_modal" class="modal fade">  
                                   <div class="modal-dialog">  
                                        <div class="modal-content">  
                                             <div class="modal-header">  
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>  
                                                  <h4 class="modal-title">Insert</h4>  
                                             </div>  
                                             <div class="modal-body">  
                                                  <form method="post" id="insert_form">  
                                                       <label>Enter Name</label>  
                                                       <input type="text" name="patient_name" id="patient_name" class="form-control" />  
                                                       <br />  
                                                       <label>Enter Email</label>  
                                                       <input type="text" name="email" id="email" class="form-control" />   
                                                       <br />  
                                                       <label>Enter Phone Number</label>  
                                                       <input type="text" name="phone_number" id="phone_number" class="form-control" />   
                                                       <br /> 
                                                       <label>Enter address</label>  
                                                       <input type="text" name="address_type" id="address_type" class="form-control" />   
                                                       <br /> 
                                                       <label>Enter Medical Condition</label>  
                                                       <input type="text" name="medical_condition" id="medical_condition" class="form-control" />   
                                                       <br /> 
                                                       <label>Enter Blood Type</label>  
                                                       <input type="text" name="blood_type" id="blood_type" class="form-control" />   
                                                       <br /> 
                                                       <input type="hidden" name="id" id="id" />  
                                                       <input type="submit" name="insert" id="insert" value="Insert" class="btn btn-success" />  
                                                  </form>  
                                             </div>  
                                             <div class="modal-footer">  
                                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                                             </div>  
                                        </div>  
                                   </div>  
                              </div>
                              <div id="div-search">
                                   Search: <input type="text" name="search" id="search" />
                              </div>
                              <?php
                              require "database.php";
                              $sql = "SELECT id, patient_name, email,phone_number,address_type,medical_condition,blood_type FROM patients";
                              $result = $pdo->query($sql);
                              ?>
                              <table class="table">
                                        <thead>
                                             <tr>
                                             <th scope="col">#</th>
                                             <th scope="col">Patient Name</th>
                                             <th scope="col">Email</th>
                                             <th scope="col">Phone Number</th>
                                             <th scope="col">Address</th>
                                             <th scope="col">Medical Condition</th>
                                             <th scope="col">Blood Type</th>
                                             <th scope="col">Action</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                             <?php
                                             if ($result->rowCount() > 0) {
                                                  // output data of each row
                                                  while($row = $result->fetch()) {
                                                  ?>
                                                       <tr>
                                                            <td><?php echo $row["id"] ?></td>
                                                            <td><?php echo $row["patient_name"] ?></td>
                                                            <td><?php echo $row["email"] ?></td>
                                                            <td><?php echo $row["phone_number"] ?></td>
                                                            <td><?php echo $row["address_type"] ?></td>
                                                            <td><?php echo $row["medical_condition"] ?></td>
                                                            <td><?php echo $row["blood_type"] ?></td>
                                                            <td><button class="btn btn-warning edit-btn" data-id="<?php echo $row["id"]; ?>">Edit</button></td>
                                                            <td><button class="btn btn-danger delete-btn" data-id="<?php echo $row["id"]; ?>">Delete</button></td>
                                                       </tr>
                                                       <?php
                                                  }
                                             }
                                             else {
                                               echo "No results";
                                             }
                                             ?>           
                                        </tbody>
                              </table>
                    </div>
               </div>
         </div>
          <!--jquery cdn-->
          <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
          <!--bootstrap-->
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
          <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>
          <script>
          $(document).ready(function(){  
          $('#add').click(function(){  
               $('#insert').val("Insert");  
               $('#insert_form')[0].reset();  
               $('#add_modal').modal('show');  
          });  
          $(document).on('click', '.edit-btn', function(){  
               var id = $(this).attr("data-id");  
               $.ajax({  
                    url:"edit_patient.php",  
                    method:"POST",  
                    data:{id:id},  
                    dataType:"json",  
                    success:function(data){  
                         $('#patient_name').val(data.patient_name);  
                         $('#email').val(data.email);  
                         $('#phone_number').val(data.phone_number);  
                         $('#address_type').val(data.address_type);  
                         $('#medical_condition').val(data.medical_condition);  
                         $('#blood_type').val(data.blood_type);  
                         $('#id').val(data.id);  
                         $('#insert').val("Update");  
                         $('#add_modal').modal('show');  
                    }  
               });  
          }); 
          $(document).on('click', '.delete-btn', function(e){ 
               e.preventDefault(); 
               var id = $(this).attr("data-id");
               $.ajax({  
                    url:"delete-patient.php",  
                    method:"POST",  
                    data:{id:id},    
                    success:function(data){  
                         $('.table').html(data);  
                    }  
               });  
          });
          $(document).on("submit",'#insert_form', function(e){  
               e.preventDefault();  
               if($('#patient_name').val() == "")  
               {  
                    alert("patient_name is required");  
               }  
               else if($('#email').val() == '')  
               {  
                    alert("Email is required");  
               }  
               else if($('#phone_number').val() == '')  
               {  
                    alert("phone_number is required");  
               }  
               else if($('#address_type').val() == '')  
               {  
                    alert("address_type is required");  
               } 
               else if($('#medical_condition').val() == '')  
               {  
                    alert("Medical Condition is required");  
               }  
               else if($('#blood_type').val() == '')  
               {  
                    alert("Blood Type is required");  
               }   
               else  
               {  
                    $.ajax({  
                         url:"insert-patient.php",  
                         method:"POST",  
                         data:$('#insert_form').serialize(),  
                         beforeSend:function(){  
                              $('#insert').val("Inserting");  
                         },  
                         success:function(data){  
                              $('#insert_form')[0].reset();  
                              $('#add_modal').modal('hide');  
                              $('.table').html(data);
                              $('#id').val('');   
                         }  
                    });  
               }  
          });
          $("#search").keyup(function() {    
                         var search = $('#search').val();
                         if (search == "") { 
                              $("#table").html("");
                         }
                         else {
                              $.ajax({
                                   type: "POST",
                                   url: "search-patient.php",
                                   data: {
                                        search: search
                                   },
                                   success: function(data) {
                                   $(".table").html(data);
                                   }
                              });
                         }
                    });

          });   
     </script>
</body>
</html>