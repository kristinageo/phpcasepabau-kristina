<?php
include "database.php";
if (isset($_POST['search'])) {
   $message = '';
   $output = '';
   $search = $_POST['search'];
   //limit 10 can be removed if we don;t limitation on the query
   $query = "SELECT id, patient_name, email,phone_number,address_type,medical_condition,blood_type FROM patients WHERE id='$search' LIMIT 10";
   $message = '<div><label class="text-success"> Search results: </label></div>';   
   echo $message;
   $output .= '  <table class="table table-bordered">  
                   <thead> 
                        <th scope="col">Id</th>
                        <th scope="col">Patient Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Phone Number</th>
                        <th scope="col">Address</th>
                        <th scope="col">Medical Condition</th>
                        <th scope="col">Blood Type</th>
                        <th width="15%">Edit</th>  
                        <th width="15%">Delete</th>  
                  </thead>  
                  <tbody>
   ';  
   if($stmt = $pdo->prepare($query)){
        if($stmt->execute()){
            if($stmt->rowCount() > 0){
                while($row = $stmt->fetch()){  
                    $output .= '  
                        <tr>  
                                <td>'. $row["id"] .'</td>
                                <td>'. $row["patient_name"] .'</td>
                                <td>'. $row["email"] .'</div></td>
                                <td>'. $row["phone_number"] .'</td>
                                <td>'. $row["address_type"] .'</td>
                                <td>'. $row["medical_condition"] .'</td>
                                <td>'. $row["blood_type"] .'</td>
                                <td><button class="btn btn-warning edit-btn" data-id="'. $row["id"] .'">Edit</button></td>
                                <td><button class="btn btn-danger delete-btn" data-id="'. $row["id"] .'">Delete</button></td>
                        </tr>  
                    ';  
                }  
            } else{
                $output .= '  
                        <tr>  
                             <td colspan="9">No results</td>
                        </tr>  
                    '; 

            }
        }  
    $output .= '</tbody></table>'; 
    }
   echo $output;
}  
?>