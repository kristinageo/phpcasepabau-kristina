
<?php  
           require "database.php"; 
           $output = '';  
           $message = ''; 
           $id=$_POST['id'];
           $query = "DELETE FROM patients WHERE id=$id";  
           $stmt= $pdo->prepare($query);
           $stmt->execute(); 
           $message_delete = '<div><label class="text-success"> Data deleted </label></div>';   
           echo $message_delete;       
           $select_query = "SELECT id, patient_name, email,phone_number,address_type,medical_condition,blood_type FROM patients";  
           $output .= '  
           <table class="table table-bordered">  
                <thead> 
                      <th scope="col">Id</th>
                       <th scope="col">Patient Name</th>
                       <th scope="col">Email</th>
                       <th scope="col">Phone Number</th>
                       <th scope="col">Address</th>
                       <th scope="col">Medical Condition</th>
                       <th scope="col">Blood Type</th>
                       <th width="15%">Edit</th>  
                       <th width="15%">Delete</th>  
                </thead>  <tbody>
          ';  
          if($stmt = $pdo->prepare($select_query)){
             if($stmt->execute()){
               if($stmt->rowCount() > 0){
                    while($row = $stmt->fetch()){                     
                        $output .= '  
                            <tr>  
                                    <td>'. $row["id"] .'</td>
                                    <td>'. $row["patient_name"] .'</td>
                                    <td>'. $row["email"] .'</div></td>
                                    <td>'. $row["phone_number"] .'</td>
                                    <td>'. $row["address_type"] .'</td>
                                    <td>'. $row["medical_condition"] .'</td>
                                    <td>'. $row["blood_type"] .'</td>
                                    <td><button class="btn btn-warning edit-btn" data-id="'. $row["id"] .'">Edit</button></td>
                                    <td><button class="btn btn-danger delete-btn" data-id="'. $row["id"] .'">Delete</button></td>
                            </tr>  
                        ';  
                     }  
                }              
            }  
            $output .= '</tbody></table>'; 
        }
      echo $output;  
 ?>
 